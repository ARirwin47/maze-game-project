# Maze Game Project

Welcome to my Maze Game Project. This GitLab repository contains all of my files for the maze project in one easy to access folder. To see my report for this project, please see the moodle assignment page.

## What This Repository Contains

This repository contains a folder with 8 files. The main file which contains the main game code, the sprites file which contains the code that draws the maze into the game, maze_model which contains the data algorithm that reads the CSV file and converts the file into intergers, test_maze_model that tests the maze_model file and finaly the CSV file and image files. This repo also contains a pycache folder but this folder does not have any major effect on how the program works.

## Requirements to Run the Game

In order to run the program, you must make sure that the following is installed on your system.

- [ ] [Visual Studio Code 2022 or equivalent program that runs python files]()
- [ ] [The latest version of Python 3.10]()
- [ ] [That pygame and pytest is installed in the python 3.10 directory using pip]()
- [ ] [The Python extension in Visual Studio Code (So you can run the python interpreter)]()

In addition to this, you must also make sure that all of the folder contents from the repository is downloaded or the program will not work!

## How to Run the Game

1. Open the project folder using visual studio code.

2. Make sure that the main.py file is open

3. Click on the play button in the top right corner of the screen.

## Known Issues

1. The enemy sprite is only moving in one direction. 

2. Wall collisions has not been implemented due to time limit on project.

3. The mechanic where the enemy is following the player has not been implemented due to time limit.

## Support
For help on this project please contact me using the following email address.

2013559@leedstrinity.ac.uk

## Authors and acknowledgment
Aiden Irwin - Project Leader

## License
For open source projects, say how it is licensed.

## Project status
At the moment, the project has not fully completed development and due to a time limit from the project owner, the project has ceased development. However you are free to continue working on this project by creating a fork of this project. If you have any questions, please contact me via email.