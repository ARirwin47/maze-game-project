import csv

class MazesData:
    """
    Contains a dictonary containing the cell and the directional nodes
    -------------------------------------------------------------------
    Returns the cell as a string and each directional nodes as int
    
    """

    def __init__(self, filename):
        self.mazes = self.load_file(filename)

    def get_maze_cells(self):
        """
        Return the list of cells (Strings)
        """
        maze_cells = list(self.mazes.keys())
        return maze_cells

    def get_maze_E(self, maze_cell):
        """
        Return the east node of the cell.
        """
        maze = self.mazes[maze_cell]
        return maze["E"]

    def get_maze_W(self, maze_cell):
        """
        Return the west node of the cell.
        """
        maze = self.mazes[maze_cell]
        return maze["W"]

    def get_maze_N(self, maze_cell):
        """
        Return the north node of the cell.
        """
        maze = self.mazes[maze_cell]
        return maze["N"]

    def get_maze_S(self, maze_cell):
        """
        Return the south node of the cell.
        """
        maze = self.mazes[maze_cell]
        return maze["S"]
    
    def load_file(self, filename):
        """
        Loads the CSV file. Return a dictionary.

        Parameters
        -----------
        filename : str
            The name of a CSV file containing the data.
            The format is: Cell, E, W, N, S
        """
        file = open(filename)
        reader = csv.reader(file)
        data = list(reader)
        mazes = {}
        for item in data[1:]:
            maze_cell = item[0]
            E = int(item[1])
            W = int(item[2])
            N = int(item[3])
            S = int(item[4])
            mazes[maze_cell] = {
                "E":E,
                "W":W,
                "N":N,
                "S":S
            }
        # return the new dict with the structured data
        return mazes