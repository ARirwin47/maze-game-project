from turtle import distance, speed
from maze_model import MazesData
from main import Enemy

test_file = "Maze.csv"

def test_get_maze_cells():
    mazes_data = MazesData(test_file)
    maze_cells = mazes_data.get_maze_cells()
    assert(maze_cells[0] == "(2, 1)")

def test_get_maze_E():
    mazes_data = MazesData(test_file)
    E = mazes_data.get_maze_E("1")
    assert (E == 1)

def test_get_maze_W():
    mazes_data = MazesData(test_file)
    W = mazes_data.get_maze_W("2")
    assert (W == 0)

def test_get_maze_N():
    mazes_data = MazesData(test_file)
    N = mazes_data.get_maze_N("3")
    assert (N == 0)

def test_get_maze_S():
    mazes_data = MazesData(test_file)
    S = mazes_data.get_maze_S("4")
    assert (S == 0)

