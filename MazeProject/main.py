# This program contains all of the code for Spawing the maze and starting the game code
# This file also contains Enemy and Level classes and player information
# Repo Link (https://gitlab.com/ARirwin47/maze-game-project/-/tree/main/)

import pygame, sys, os
from pygame.image import load
from maze_model import MazesData
from sprites import Cell

data_file = "Maze.csv" #Loads the .CSV file

######### Variables ###########
incr = [0, 0]
xs, ys = 400, 300

########## Maze Spawn Utility ##########
# Loads the maze

def load_mazes(filename):
    mazes = pygame.sprite.Group()
    maze_data = MazesData(data_file)
    maze_cells = maze_data.get_maze_cells()

    for maze_cell_id in maze_cells:
      # Screen size is 700x700 so x and y must be times by 70
      east = maze_data.get_maze_E(maze_cell_id)
      west = maze_data.get_maze_W(maze_cell_id)
      north = maze_data.get_maze_N(maze_cell_id)
      south = maze_data.get_maze_S(maze_cell_id)
      # Takes two numbers from each cell and converts both numbers from one string
      # into two integers, one for the x position and another int for the y position
      x = int(maze_cell_id.split(",")[1].rstrip(")"))  # Extract the position of x (Column)
      y = int(maze_cell_id.split(",")[0].lstrip("("))  # Extract the position of y (Row)
      x = (x - 1) * 70
      y = (y - 1) * 70
      m = Cell(east, west, north, south, x, y)
      mazes.add(m)
    return mazes
    
########## Game Classes ############

class Enemy(pygame.sprite.Sprite):
    '''
    Contains all of the code for the enemy sprite.
    ------------------------------------------------
    This class includes a function to set up the class
    and a function to allow the enemy to move.
    '''
    def __init__(self, X, Y, img):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(os.path.join("Images", img))
        self.rect = self.image.get_rect()
        self.rect.x = X
        self.rect.y = Y
        self.counter = 0
    
    def enemy_move(self):
        '''
        Enemy auto movement in maze
        ---------------------------
        Still working on it :)
        '''
        distance = 70
        speed = 3
        
        if self.counter >= 0 and self.counter <= distance:
            self.rect.x += speed
        elif self.counter >= distance and self.counter <= distance*2:
            self.rect.x -= speed
        else:
            self.counter = 0
        
        self.counter += 1

class Level():
    '''
    Contains code that adds the enemy to the level
    ----------------------------------------------
    Uses the Enemy class to add the enemy to the maze
    '''
    def bad(lvl, eloc):
        if lvl == 1:
            enemy_base = Enemy(eloc[0], eloc[1], "square-1.jpg")
            enemy_list = pygame.sprite.Group()
            enemy_list.add(enemy_base)
        if lvl == 2:
            print("Level" + str(lvl))
        
        return enemy_list

########## Game Code ##########

pygame.init()

screen_width, screen_height = 700, 700
maze_group = load_mazes("Maze.csv")

clock = pygame.time.Clock()

screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption("The Attack of the Red Squares!")

# Positional information for enemy sprite
eloc = []
eloc = [300, 300]
enemy_list = Level.bad(1, eloc)

# Game Loop
while True:
    # Adds White background and draws player using pygame.Surface
    screen.fill((255, 255, 255))
    NewPlayer = pygame.image.load("green.jpg")
    
    # detect events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            sys.exit()

        # Allows the user to move the player using the arrow keys
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT or event.key == ord("a"):
                incr[0] = -3
            if event.key == pygame.K_RIGHT or event.key == ord("d"):
                incr[0] = 3
            if event.key == pygame.K_UP or event.key == ord("w"):
                incr[1] = -3
            if event.key == pygame.K_DOWN or event.key == ord("s"):
                incr[1] = 3
        # If key is unpressed, the player stops moving
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == ord("a"):
                incr[0] = 0
            if event.key == pygame.K_RIGHT or event.key == ord("d"):
                incr[0] = 0
            if event.key == pygame.K_UP or event.key == ord("w"):
                incr[1] = 0
            if event.key == pygame.K_DOWN or event.key == ord("s"):
                incr[1] = 0
        
    xs = xs + incr[0]
    ys = ys + incr[1]

    # Draws the maze to the screen
    for m in maze_group:
        m.draw(screen)
    
    # Draws the player sprite
    screen.blit(NewPlayer,(xs, ys))
    
    # Draws the enemy sprites
    enemy_list.draw(screen)
    for e in enemy_list:
        e.enemy_move()
    
    pygame.display.update()
    clock.tick(60)