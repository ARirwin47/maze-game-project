import pygame
from pygame.sprite import Sprite
from pygame.math import Vector2

black = (0, 0, 0)

class Cell(Sprite):
    '''
    Contains code to draw the maze into pygame.
    ---------------------------------------------
    Draws the maze using a data algorithm that converts
    the nodes in a CSV file into lines.
    '''
    # This function just initialises the class
    def __init__(self, E, W, N, S, x, y):
        super().__init__()
        self.connection = [E, W, N, S]
        self.position = Vector2(x, y)
        self.rect = pygame.Rect(x, y, 70, 70)
        self.colour = black
        
    # This function draws the lines in the pygame window using the E, W, N and S nodes
    # And using the x and y positions converted from the cell column in the CSV file
    # Some sections of Self.position.x and self.position.y is added by 70 due to the game
    # window being 700 by 700
    def draw(self, target):
        
        if self.connection[0] == 0:
            start = [self.position.x + 70, self.position.y]
            end = [self.position.x + 70, self.position.y + 70]
            pygame.draw.line(target, self.colour, start, end)
        
        if self.connection[1] == 0:
            start = [self.position.x, self.position.y]
            end = [self.position.x, self.position.y + 70]
            pygame.draw.line(target, self.colour, start, end)
        
        if self.connection[2] == 0:
            start = [self.position.x, self.position.y]
            end = [self.position.x + 70, self.position.y]
            pygame.draw.line(target, self.colour, start, end)
        
        if self.connection[3] == 0:
            start = [self.position.x, self.position.y + 70]
            end = [self.position.x + 70, self.position.y + 70]
            pygame.draw.line(target, self.colour, start, end)


class PlayerOne(Sprite):
    '''
    Contains the code for the player sprite
    ----------------------------------------
    Includes code for moving the player sprite
    and sprite collisions with the maze walls
    '''
    def __init__(self):
        super().__init__()
        self.position = Vector2(400, 300)
        self.direction = Vector2(0, 0)

